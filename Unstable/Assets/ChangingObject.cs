using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ChangingObject : MonoBehaviour
{
    private Sprite baseSprite;
    private Coroutine changeCoroutine;

    public BoxCollider2D body;
    public SpriteRenderer spriteRenderer;

    private float stableCountdown = 0;
    public HashSet<Stabilizer> stablizerContacts = new HashSet<Stabilizer>();
    public bool forceStable = false;

    // Start is called before the first frame update
    void Start()
    {
        baseSprite = spriteRenderer.sprite;
        changeCoroutine = StartCoroutine(ChangeObject());
    }

    private void FixedUpdate()
    {
        if(IsStable())
        {
            if (changeCoroutine != null)
            {
                StopCoroutine(changeCoroutine);
                changeCoroutine = null;
            }
            spriteRenderer.color = GetObjectColour();
            spriteRenderer.sprite = GetBaseSprite();
            StabilizeObject();
        }
        else if(changeCoroutine == null)
        {
            changeCoroutine = StartCoroutine(ChangeObject());
        }
       
        float dissapateTime = 0.4f;
        if(forceStable)
        {
            spriteRenderer.transform.localPosition = Vector2.zero;
        }
        else if(stableCountdown > 0)
        {
            stableCountdown -= Time.fixedDeltaTime;
            Vector2 randPos = (Random.insideUnitSphere * 0.2f * (stableCountdown - 1f));
            spriteRenderer.transform.localPosition = randPos;
        }
        else if (stablizerContacts.Count == 0 && GameHandler.gameHandler.StableTimeLeftPercentage() < dissapateTime)
        {
            Vector2 randPos = (Random.insideUnitSphere * 0.1f * ((dissapateTime - GameHandler.gameHandler.StableTimeLeftPercentage()) / dissapateTime));
            spriteRenderer.transform.localPosition = randPos;
        }
        else
        {
            //spriteRenderer.transform.localPosition = Vector2.zero;
            Vector2 randPos = (Random.insideUnitSphere * 0.05f);
            spriteRenderer.transform.localPosition = randPos;
        }
    }

    private IEnumerator ChangeObject()
    {
        DestabilizeObject();

        Color objColour = GetObjectColour();
        objColour.a = Random.Range(0.2f, 0.6f);
        spriteRenderer.color = objColour;
        Sprite currSprite = spriteRenderer.sprite;
        while (spriteRenderer.sprite == currSprite)
        {
            spriteRenderer.sprite = GetUnstableSprite();
        }
        yield return new WaitForSeconds(0.15f);
        changeCoroutine = StartCoroutine(ChangeObject());
    }

    protected virtual Sprite GetBaseSprite()
    {
        return baseSprite;
    }

    protected virtual bool IsStable()
    {
        return GameHandler.gameHandler.IsStable() || stablizerContacts.Count > 0 || stableCountdown > 0 || forceStable;
    }

    protected abstract Sprite GetUnstableSprite();
    protected abstract Color GetObjectColour();

    protected abstract void StabilizeObject();

    protected abstract void DestabilizeObject();

    public void AddStabilizer(Stabilizer stabilizer)
    {
        stablizerContacts.Add(stabilizer);
        stableCountdown = 0f;
    }

    public void RemoveStabilizer(Stabilizer stabilizer)
    {
        int prevCount = stablizerContacts.Count;
        stablizerContacts.Remove(stabilizer);
        if(prevCount > 0 && stablizerContacts.Count == 0)
        {
            stableCountdown = 1f;
        }
    }
}
