using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform playerTransform;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float worldHeight = Camera.main.orthographicSize * 2.0f;
        float halfHeight = worldHeight * 0.5f;
        float worldWidth = worldHeight * Camera.main.aspect;
        float halfWidth = worldWidth * 0.5f;

        float newPosX = (1 + Mathf.FloorToInt((playerTransform.position.x - halfWidth) / worldWidth)) * worldWidth;
        float newPosY = (1 + Mathf.FloorToInt((playerTransform.position.y - halfHeight) / worldHeight)) * worldHeight;

        Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, new Vector3(newPosX, newPosY, -10), 50f * Time.deltaTime);
    }
}
