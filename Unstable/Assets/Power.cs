using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Power : ChangingObject
{
    public abstract bool IsProducingPower();
}
