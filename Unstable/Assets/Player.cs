using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody2D body;
    public Transform groundCheck;

    private bool isGrounded = true;

    public Checkpoint storedCheckpoint = null;

    private bool hasDevice = false;

    private float timeSinceLastMovement = 0;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if ((Input.GetButton("Jump")) && isGrounded)
        {
            body.AddForce(new Vector2(0, 950));
            isGrounded = false;
        }

        if(hasDevice && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            if (!GameHandler.gameHandler.IsStable())
            {
                GameHandler.gameHandler.ActivateStability();
            }
            /*else
            {
                GameHandler.gameHandler.DeactivateStability();
            }*/
        }

        float moveX = Input.GetAxisRaw("Horizontal") * 7f;
        body.velocity = new Vector2(moveX, body.velocity.y);

        if(transform.position.y < -20)
        {
            if(storedCheckpoint != null)
            {
                transform.position = storedCheckpoint.respawnPos.position;
            }
            else
            {
                transform.position = Vector2.zero;
            }
        }

        
        if(GameHandler.gameHandler.IsStable() || body.velocity.magnitude != 0)
        {
            timeSinceLastMovement = Time.time;
        }

        if (!GameHandler.gameHandler.IsStable() && GameHandler.gameHandler.currentPower < 3 && (timeSinceLastMovement + 1) < Time.time)
        {
            GameHandler.gameHandler.currentPower++;
            timeSinceLastMovement = Time.time;
        }
    }

    private void LateUpdate()
    {
        if (Mathf.Approximately(body.velocity.y, 0f))
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, 0.5f);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    isGrounded = true;
                }
            }
        }
    }

    public void EquipDevice()
    {
        hasDevice = true;
        GameHandler.gameHandler.DisplayDeviceCanvas();
    }
}
