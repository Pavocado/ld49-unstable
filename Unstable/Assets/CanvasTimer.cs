using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasTimer : MonoBehaviour
{
    public Image timerLeft;
    public Image timerTopLeft;
    public Image timerBottomLeft;
    public Image timerRight;
    public Image timerTopRight;
    public Image timerBottomRight;

    public Image flashImage;

    private Coroutine flashRoutine;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float totalSize = (Screen.width + Screen.height - 62);
        float percentageRemaining = 1 - GameHandler.gameHandler.StableTimeLeftPercentage();

        float topTimerSize = (Screen.width / 2f) - (totalSize * percentageRemaining);
        timerTopLeft.rectTransform.sizeDelta = new Vector2(topTimerSize, 16);
        timerTopRight.rectTransform.sizeDelta = new Vector2(topTimerSize, 16);

        float sideTimerSize = -22 + Screen.height + (Screen.width / 2f) - (totalSize * percentageRemaining);
        timerLeft.rectTransform.sizeDelta = new Vector2(16, sideTimerSize);
        timerRight.rectTransform.sizeDelta = new Vector2(16, sideTimerSize);

        float bottomTimerSize = -62 + Screen.height + Screen.width - (totalSize * percentageRemaining);
        bottomTimerSize = Mathf.Min(bottomTimerSize, Screen.width / 2f);
        timerBottomLeft.rectTransform.sizeDelta = new Vector2(bottomTimerSize, 16);
        timerBottomRight.rectTransform.sizeDelta = new Vector2(bottomTimerSize, 16);
        timerBottomLeft.rectTransform.anchoredPosition = new Vector3(-40 + (Screen.width / 2f) - bottomTimerSize, 22, 0);
        timerBottomRight.rectTransform.anchoredPosition = new Vector3(40 + bottomTimerSize - (Screen.width / 2f), 22, 0);
    }

    public void FlashCamera()
    {
        if (flashRoutine == null)
        {
            flashRoutine = StartCoroutine(Flash());
        }
    }

    private IEnumerator Flash()
    {
        for(int i = 10; i >= 0; i--)
        {
            flashImage.color = new Color(1, 1, 1, 0.07f * i);
            yield return new WaitForSeconds(0.05f);
        }
        flashRoutine = null;
    }
}
