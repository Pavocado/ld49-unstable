using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class Circuitry : MonoBehaviour
{
    public Power power;
    public Stabilizer stabilizer;

    public List<Transform> waypointPos = new List<Transform>();

    public LineRenderer lineRenderer;

    private void Update()
    {
        List<Vector3> nodes = new List<Vector3>();
        nodes.Add(power.transform.position);
        foreach(Transform t in waypointPos)
        {
            nodes.Add(t.position);
        }
        nodes.Add(stabilizer.transform.position);

        lineRenderer.SetPositions(nodes.ToArray());
        lineRenderer.positionCount = nodes.Count;
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;
    }

    public void UpdatePower(bool isActive)
    {
        stabilizer.isActive = isActive;
        if (!isActive)
        {
            lineRenderer.startColor = new Color(48 / 255f, 48 / 255f, 48 / 255f);
            lineRenderer.endColor = new Color(48 / 255f, 48 / 255f, 48 / 255f);
        }
        else
        {
            lineRenderer.startColor = new Color(212 / 255f, 212 / 255f, 212 / 255f, 140/255f);
            lineRenderer.endColor = new Color(212 / 255f, 212 / 255f, 212 / 255f, 140 / 255f);

        }
    }
}
