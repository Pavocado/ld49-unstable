using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public List<Transform> stops = new List<Transform>();

    private Vector2 currentTarget;

    // Start is called before the first frame update
    void Start()
    {
        currentTarget = stops[0].position;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        transform.position = Vector2.MoveTowards(transform.position, currentTarget, 2f * Time.fixedDeltaTime);
        if(Vector2.Distance(transform.position, currentTarget) < 0.1f)
        {
            Transform targetTransform = stops[0];
            currentTarget = targetTransform.position;
            stops.RemoveAt(0);
            stops.Add(targetTransform);
        }
    }
}
