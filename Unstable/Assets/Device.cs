using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Device : MonoBehaviour
{
    private void Update()
    {
        if (Vector2.Distance(GameHandler.gameHandler.player.transform.position, transform.position) < 1f)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                GameHandler.gameHandler.player.EquipDevice();
                Destroy(gameObject);
            }
        }
    }
}
