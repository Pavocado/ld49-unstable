using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkingRadius : MonoBehaviour
{
    public SpriteRenderer sprite;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.localScale.x > 1)
        {
            float currScale = transform.localScale.x;
            currScale -= (Time.fixedDeltaTime * currScale * 0.1f);

            transform.localScale = new Vector3(currScale, currScale, currScale);
        }
    }

    public bool IsStable()
    {
        return transform.localScale.x <= 1;
    }
}
