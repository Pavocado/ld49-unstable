using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : Power
{
    public List<Circuitry> circuitry = new List<Circuitry>();

    protected override Sprite GetUnstableSprite()
    {
        if(Random.Range(0, 3) == 0)
        {
            return GetBaseSprite();
        }
        return GameHandler.gameHandler.GetRandomUnstableSprite(ObjectEnum.GENERATOR);
    }
    protected override Color GetObjectColour()
    {
        return new Color(195 / 255f, 242 / 255f, 186 / 255f);
    }

    private void Update()
    {
        foreach (Circuitry circuit in circuitry)
        {
            circuit.UpdatePower(IsProducingPower());
        }
    }

    protected override void StabilizeObject()
    {
        gameObject.layer = LayerMask.NameToLayer("Default");
    }

    protected override void DestabilizeObject()
    {
        gameObject.layer = LayerMask.NameToLayer("NonSolid");
    }

    public override bool IsProducingPower()
    {
        return IsStable();
    }
}

