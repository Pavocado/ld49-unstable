using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    public Player player;

    private float timerMax = 6;
    public float currentStableTime = 0;
    public int currentPower = 3;

    public List<Sprite> unstableSpriteList = new List<Sprite>();

    public List<Sprite> platformSpriteList = new List<Sprite>();
    public List<Sprite> leverSpriteList = new List<Sprite>();
    public List<Sprite> generatorSpriteList = new List<Sprite>();

    public Canvas batteryCanvas;

    public Sprite battery0;
    public Sprite battery1;
    public Sprite battery2;
    public Sprite battery3;

    public Image batteryImage;
    public CanvasTimer canvasTimer;

    public static GameHandler gameHandler;

    // Start is called before the first frame update
    void Awake()
    {
        batteryCanvas.enabled = false;
        gameHandler = this;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(currentStableTime > 0)
        {
            currentStableTime -= Time.fixedDeltaTime;
        }

        switch(currentPower)
        {
            case 0: 
                batteryImage.sprite = battery0;
                break;
            case 1:
                batteryImage.sprite = battery1;
                break;
            case 2:
                batteryImage.sprite = battery2;
                break;
            case 3:
                batteryImage.sprite = battery3;
                break;
            default:
                batteryImage.sprite = battery0;
                break;
        }
    }

    public void ActivateStability()
    {
        if (!IsStable() && currentPower > 0)
        {
            canvasTimer.FlashCamera();
            currentStableTime = currentPower * (timerMax / 3f);
            currentPower = 0;
        }
    }

    public void DeactivateStability()
    {
        currentStableTime = 0;
    }

    public bool IsStable()
    {
        return currentStableTime > 0;
    }

    public float StableTimeLeftPercentage()
    {
        return (currentStableTime / timerMax);
    }

    public Sprite GetRandomUnstableSprite(ObjectEnum objEnum)
    {
        switch(objEnum)
        {
            case ObjectEnum.PLATFORM:
                return platformSpriteList[Random.Range(0, platformSpriteList.Count)];
            case ObjectEnum.LEVER:
                return leverSpriteList[Random.Range(0, leverSpriteList.Count)];
            case ObjectEnum.GENERATOR:
                return generatorSpriteList[Random.Range(0, generatorSpriteList.Count)];
            default:
                return unstableSpriteList[Random.Range(0, unstableSpriteList.Count)];
        }
    }

    public void DisplayDeviceCanvas()
    {
        batteryCanvas.enabled = true;
    }
}
