using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : ChangingObject
{
    protected override Sprite GetUnstableSprite()
    {
        return GameHandler.gameHandler.GetRandomUnstableSprite(ObjectEnum.PLATFORM);
    }
    protected override Color GetObjectColour()
    {
        return new Color(1, 1, 1);
    }

    protected override void StabilizeObject()
    {
        gameObject.layer = LayerMask.NameToLayer("Default");
    }

    protected override void DestabilizeObject()
    {
        gameObject.layer = LayerMask.NameToLayer("NonSolid");
    }
}
