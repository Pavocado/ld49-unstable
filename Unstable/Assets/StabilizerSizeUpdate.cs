using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class StabilizerSizeUpdate : MonoBehaviour
{
    private CircleCollider2D col;
    private Stabilizer stabilizer;
    private Transform spritesTransform;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<CircleCollider2D>();
        stabilizer = GetComponent<Stabilizer>();
        spritesTransform = transform.Find("Sprites");
    }

    // Update is called once per frame
    void Update()
    {
        if(stabilizer && col)
        {
            col.radius = stabilizer.radius;
            float spriteSize = (1/2.8f) * stabilizer.radius;
            spritesTransform.localScale = new Vector3(spriteSize, spriteSize, spriteSize);
        }
    }
}
