using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : Power
{
    public bool isActive = false;

    public Sprite onSprite;
    public Sprite offSprite;

    public List<Circuitry> circuitry = new List<Circuitry>();

    protected override Sprite GetUnstableSprite()
    {
        if(Random.Range(0, 3) == 0)
        {
            return GetBaseSprite();
        }
        return GameHandler.gameHandler.GetRandomUnstableSprite(ObjectEnum.LEVER);
    }
    protected override Color GetObjectColour()
    {
        return new Color(243 / 255f, 196 / 255f, 185 / 255f);
        //return new Color(185 / 255f, 210 / 255f, 243 / 255f);
    }

    private void Update()
    {
        if(Vector2.Distance(GameHandler.gameHandler.player.transform.position, transform.position) < 1f)
        {
            if (IsStable() && Input.GetKeyDown(KeyCode.X))
            {
                isActive = !isActive;
                spriteRenderer.sprite = GetBaseSprite();
            }
        }
        foreach (Circuitry circuit in circuitry)
        {
            circuit.UpdatePower(IsProducingPower());
        }
    }

    protected override Sprite GetBaseSprite()
    {
        return isActive ? onSprite : offSprite;
    }

    protected override void StabilizeObject()
    {
        gameObject.layer = LayerMask.NameToLayer("NonSolid");
    }

    protected override void DestabilizeObject()
    {
        gameObject.layer = LayerMask.NameToLayer("NonSolid");
    }

    public override bool IsProducingPower()
    {
        return isActive;
    }
}

