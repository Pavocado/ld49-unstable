using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stabilizer : MonoBehaviour
{
    public CircleCollider2D circleCol;
    public bool isActive = false;
    public SpriteRenderer ringSprite;
    public SpriteRenderer bubbleSprite;
    public SpriteRenderer spriteRenderer;

    public float radius = 2.8f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isActive)
        {
            ChangingObject co = collision.GetComponent<ChangingObject>();
            if (co)
            {
                co.AddStabilizer(this);
            }
        }
    }

    private void Update()
    {
        ringSprite.enabled = isActive;
        bubbleSprite.enabled = isActive;
        circleCol.enabled = isActive;

        if (isActive)
        {
            TurnOn();
            spriteRenderer.color = Color.white;

        }
        else
        {
            TurnOff();
            spriteRenderer.color = new Color(113 / 255f, 113 / 255f, 113 / 255f);
        }
    }

    public void TurnOn()
    {
        Collider2D[] allOverlappingColliders = Physics2D.OverlapCircleAll(transform.position, radius);
        foreach(Collider2D collision in allOverlappingColliders)
        {
            ChangingObject co = collision.GetComponent<ChangingObject>();
            if (co)
            {
                co.AddStabilizer(this);
            }
        }
    }

    public void TurnOff()
    {
        Collider2D[] allOverlappingColliders = Physics2D.OverlapCircleAll(transform.position, radius);
        foreach (Collider2D collision in allOverlappingColliders)
        {
            ChangingObject co = collision.GetComponent<ChangingObject>();
            if (co)
            {
                co.RemoveStabilizer(this);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        ChangingObject co = collision.GetComponent<ChangingObject>();
        if (co)
        {
            co.RemoveStabilizer(this);
        }
    }
}
